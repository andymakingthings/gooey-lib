//
//  math.hpp
//  gooey-lib
//

#ifndef gooey_lib_math_hpp
#define gooey_lib_math_hpp

/* let's not pollute the namespace! ***************************************** */
/* all this function template needs is for less to be defined *************** */
namespace gooey::math {

template <typename T, typename A>
T fix_within_minimum(T x, A min)
{
	return x<min?min:x;
}

template <typename T, typename A>
T fix_within_maximum(T x, A max)
{
	return x<max?x:max;
}

template <typename T, typename A, typename B>
T fix_within_bounds(T x, A a, B b)
{
	// a is min, b is max
	if (a < b) return x<a?a:x<b?x:b;
	return x<b?b:x<a?x:a;
}

template <typename T>
int sign(T x)
{
	return 0 < x ? 1 : x < 0 ? -1 : 0;
}

} /* end namespace gooey::math ********************************************* */
#endif
