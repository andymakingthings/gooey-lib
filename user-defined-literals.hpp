//
//  user-defined-literals.hpp
//  gooey-lib
//

#ifndef gooey_lib_user_defined_literals_h
#define gooey_lib_user_defined_literals_h

#include <string>

/* a tasty little bit of syntactic sugar for user defined macros - might not  */
/* be as necessary when declytype(auto) is implemented! ********************* */
#define GOOEY_LITERAL_DEFINE( _SUFFIX_, _ARGNAME_, _RETURNTYPE_ ) \
namespace gooey { namespace user_defined_literals { \
    struct _SUFFIX_##_Functionoid \
    { static const _RETURNTYPE_ literal_transform(const std::string& _ARGNAME_); }; \
}} \
\
auto operator ""_##_SUFFIX_ (const char* p, size_t n) -> \
    decltype( gooey::user_defined_literals::_SUFFIX_##_Functionoid::literal_transform('"'+std::string(p,n)+'"') ) \
    { return gooey::user_defined_literals::_SUFFIX_##_Functionoid::literal_transform('"'+std::string(p,n)+'"'); } \
\
const _RETURNTYPE_ gooey::user_defined_literals::_SUFFIX_##_Functionoid::literal_transform(const std::string& _ARGNAME_) \

#define GOOEY_LITERAL_IMPLEMENT( _SUFFIX_ ) \
template <char... CS> auto operator""_##_SUFFIX_() -> \
    decltype(gooey::user_defined_literals::user_defined_literal< gooey::user_defined_literals::_SUFFIX_##_Functionoid, CS...>::literal_transform()) \
    {return gooey::user_defined_literals::user_defined_literal< gooey::user_defined_literals::_SUFFIX_##_Functionoid, CS...>::literal_transform(); } \

/* use like this: *********************************************************** */
/*
 // usage: int p = 90_degree == Pi/2
 GOOEY_LITERAL_DEFINE( degree, literal_string, long double )
 {
     long double Pi = 3.14;
     return boost::lexical_cast<long double>(literal_string)/180*Pi;
 }
 // other code if you want. DEFINE and IMPLEMENT don't need to be together, but
 // IMPLEMENT must be preceded by DEFINE.
 GOOEY_LITERAL_IMPLEMENT( degree ); // semicolon is optional
/* */
/* ************************************************************************** */


/* let's not pollute the namespace! ***************************************** */
namespace gooey::user_defined_literals {
/* ********************************************************************** */
/* grab the string from the user-defined literal ************************ */
inline void literal_grab(std::string& stuff) {}  // you need this
template <typename type_head, typename... type_tail>
void literal_grab(std::string& stuff, const type_head& head, const type_tail&... tail)
	{ stuff += head; literal_grab(stuff, tail...); }
/* ********************************************************************** */
/* use this class when creating new user-defined literals *************** */
template <typename FN, char... args> struct user_defined_literal
{	/* a pointer is cheap, need it for trailing return type ************* */
	static std::string const * const placeholder;
	static auto literal_transform()
	-> decltype(FN::literal_transform(*placeholder))
	{	// stuff a string with the literal
		std::string stuff; literal_grab(stuff, args...);
		// convert the string however we will
		return FN::literal_transform(stuff);
	}
	/* string_literal: true if given "string"_suffix format, otherwise ** */
	/* false. string is returned exactly as-is, with or without quotes ** */
	
};
/* ********************************************************************** */
/* create a class to define the conversion function from the user-defined */
/* literal string to the desired result. function must have the signature */
/* _literal_transform(const std::string&). Example below: *************** */
/*
 struct _literal_degree_Functionoid
 {
	static long double literal_transform(const std::string& literal_string);
	{
		return boost::lexical_cast<long double>(stuff) * 3.14/180;
	}
 } ;
 /* */
/* overload (implement) the user-defined string literal operator using ** */
/* the format below: **************************************************** */
/*
 template <char... CS> auto operator""_degree() -> decltype(gooey::user_defined_literals::user_defined_literal< degree_Functionoid, CS...>::literal_transform()) { return gooey::user_defined_literals::user_defined_literal< degree_Functionoid, CS...>::literal_transform(); }
 /* */
/* also define a function for the form auto s = "some string"_suffix; *** */
/*
 auto operator ""_degree (const char* p, size_t n) -> decltype(degree_Functionoid::literal_transform(std::string(p,n)))
 { return degree_Functionoid::literal_transform(std::string(p,n)); }
 /* */
	
} /* end namespace gooey::user_defined_literals **************************** */

#endif
