//
//  user-defined-literals.cpp
//  gooey-lib
//

#include <iostream>
#include <gooey-lib/user-defined-literals.hpp>
#include <cmath>

struct myclass { std::string val; };

GOOEY_LITERAL_DEFINE(mystr, stuff, myclass)
{ myclass out; out.val = stuff; return out; }
GOOEY_LITERAL_IMPLEMENT(mystr);

GOOEY_LITERAL_DEFINE(b, stuff, int)
{
    int out = 0;
    for(auto c = stuff.length(); c>0; c--)
        out += std::pow(2,stuff.length()-c) * (stuff[c-1]-48);
    return out;
}
GOOEY_LITERAL_IMPLEMENT(b);


int main(int argc, const char * argv[])
{
	std::cout << "I come with quotes included!"_mystr.val << std::endl;
	std::cout << (0x2388759_mystr).val << std::endl;
	std::cout << 1011001_b << std::endl;
	int c = 1011001_b;
	return 0;
}

