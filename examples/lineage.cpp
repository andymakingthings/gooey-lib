//
//  main.cpp
//  lineage
//

#include <gooey-lib/lineage.hpp>
#include <iostream>
#include <cmath>

class pear
{
public:
	unsigned long long x;
	unsigned long long y;
	pear() = default;
	pear(unsigned long long x_, unsigned long long y_) : x{x_}, y{y_} {}
};

int main(int argc, const char * argv[]) {

	unsigned long long runs = 1;
	long double avg_long{0};
	for (int j = 0; j < runs; ++j)
	{
		std::list<pear> listy;
		for (int i = 1; i <= 30; ++i) listy.emplace_back(pear(i,i*i));
		listy.back().x=3000;

		//auto L = gooey::lineage::fitness_proportional_selection(listy, [](pear i){return std::pow(2,i.y);}, 10);
		auto L = gooey::lineage::stochastic_universal_sampling(listy, [](pear i){return i.x;}, 10);
		
		if(runs==1) std::cout << "winners:\n";
		if(runs==1) for (auto &i : L) std::cout << "x: " << (*i).x << "\t y: " << (*i).y << "\n";
		
		long double avg{0};
		
		for (auto &i : L) avg += (*i).x;
		avg /= 10;
		if(runs==1) std::cout << "average: " << avg << "\n";
		avg_long+=avg;
	}
	avg_long /= runs;
	std::cout << "long term average: " << avg_long << "\n";

	
    return 0;
}
