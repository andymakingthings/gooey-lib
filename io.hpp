//
//  selection.hpp
//  gooey-lib
//

#ifndef gooey_lib_io_hpp
#define gooey_lib_io_hpp

#include <iomanip>
#include <sstream>

/* let's not pollute the namespace! ***************************************** */
namespace gooey::io {

inline std::string serialize(const double & x, const bool & full_precision = false)
{
	std::stringstream s;
	if (full_precision)
		s << std::setprecision(std::numeric_limits<int>::max());
	else
		s << std::setprecision(std::numeric_limits<double>::digits10+2);
	s << x;
	return s.str();
}
	
inline std::string serialize(const unsigned long long int & x, const bool & full_precision = false)
{
	std::stringstream s;
	s << std::setprecision(std::numeric_limits<unsigned long long int>::digits10+2);
	s << x;
	return s.str();
}
	
inline std::string serialize(const bool & x, const bool & full_precision = false)
{
	std::string s = x ? "true" : "false";
	return s;
}

} /* end namespace gooey::io *********************************************** */

#endif
