//#include <gooey-lib/cppmath.hpp>
#include <iostream>
#include <gooey-lib/io.hpp>
#include <gooey-lib/lineage.hpp>
#include <string>
#include <nlohmann/json.hpp>
using json = nlohmann::json;

int main()
{
	double i = 5.01;
	std::string partial = gooey::io::serialize(i);
	std::string full = gooey::io::serialize(i, true);
	std::cout << partial << std::endl;
	std::cout << full << std::endl;
	double i_p = std::stod(partial);
	double i_f = std::stod(full);
	std::cout << gooey::io::serialize(i_p, true) << std::endl;
	std::cout << gooey::io::serialize(i_f, true) << std::endl;
	if (i_p == i_f && i_f == i)
		std::cout << "shit checks out" << std::endl;
	
	gooey::lineage::Mutable_Range x;
	x.set(.501);
	std::cout << x.to_json() << std::endl;
	
	json j = json::parse(x.to_json());
	std::cout << j["p_overflow"] << std::endl;
	std::cout << x.from_json(x.to_json()).to_json() << std::endl;
}
