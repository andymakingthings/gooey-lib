//
//  misc.hpp
//  gooey-lib
//

#ifndef gooey_lib_misc_h
#define gooey_lib_misc_h

#include <fstream>
#include <vector>

/* let's not pollute the namespace! ***************************************** */
namespace gooey::misc {

/* C-style arguments are annoying. This turns them into a vector of strings.  */
inline std::vector<std::string> convert_main_args (int argc, char const * argv[])
{
	std::vector<std::string> arguments;
	for (int i = 0; i < argc; ++i) arguments.emplace_back(argv[i]);
	return arguments;
}

/* std::string replacement for unsafe cstring-based std::getenv(char *) ***** */
inline std::string get_env(const std::string &in)
{
	char * cstring = std::getenv(in.c_str());
	if(cstring != nullptr) return std::string(cstring);
	return std::string("");
}

inline std::string slurp_textfile(const std::string &fileName)
{
	std::ifstream ifs(fileName.c_str(), std::ios::in | std::ios::binary | std::ios::ate);
	auto fileSize = ifs.tellg();
	if (fileSize >= 0) // if file exists
	{
		auto i = static_cast<unsigned long long>(std::abs(fileSize));
		ifs.seekg(0, std::ios::beg);
		std::string file(i,'\0');
		ifs.read(&file[0], fileSize);
		return file;
	}
	/* returns empty string if file does not exist ************************** */
	return std::string("");
}

inline bool belch_textfile(const std::string &fileName, const std::string &content)
{
	std::ofstream ofs( fileName.c_str(), std::ios::out | std::ios::trunc | std::ios::binary );
	ofs << content;
	ofs.close();
	return true;
	/* returns false if file does not exist ********************************* */
	//return false;
}

} /* end namespace gooey::misc ********************************************* */
#endif
