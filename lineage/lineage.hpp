//
//  lineage.hpp
//  gooey-lib
//
//  Created by Andy Carlos on 11/13/14.
//  Copyright (c) 2014 Andy Carlos. All rights reserved.
//

#ifndef gooey_lib_lineage_hpp
#define gooey_lib_lineage_hpp

#include <gooey-lib/lineage/selection.hpp>
#include <gooey-lib/lineage/mutable-range.hpp>


#endif

