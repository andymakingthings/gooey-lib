//
//  mutable-probability.hpp
//  gooey-lib
//

#ifndef gooey_lib_mutable_range_h
#define gooey_lib_mutable_range_h

#include <gooey-lib/math.hpp>
#include <gooey-lib/io.hpp>
#include <nlohmann/json.hpp>
#include <limits>

/* let's not pollute the namespace! ***************************************** */
namespace gooey::lineage {

class Mutable_Range
{
private:
	// base is [0, 1]
	double base {0};
	double p_overflow {0};
	
	// default is [0, 1]
	double scale_min {0};
	double scale_max {1};
public:
	Mutable_Range() = default;
	Mutable_Range(double);
	Mutable_Range(Mutable_Range const &);
	
	double get_base() const;
	void set_base(double);
	
	double get() const;
	operator double() const;
	void set(double);
	
	void set_min(double);
	void set_max(double);
	
	double multiply(double multiplicand, bool overflow);
	double add(double addend, bool overflow);
	
	static double product_walk_probability(double probability, double factor);
	static double sum_walk_probability(double probability, double addend_factor);
	
	double product_walk(double factor);
	double sum_walk(double addend_factor);
	
	static Mutable_Range from_json(nlohmann::json const &);
	static Mutable_Range from_json(std::string const &);
	std::string to_json();
};

inline Mutable_Range::Mutable_Range(double p)
{
	set(p);
}

inline Mutable_Range::Mutable_Range(Mutable_Range const & copy_from)
	:	base(copy_from.base), p_overflow(copy_from.p_overflow),
		scale_min(copy_from.scale_min), scale_max(copy_from.scale_max) {}

inline double Mutable_Range::get_base() const
{
	return base;
}

inline double Mutable_Range::get() const
{
	return base * (scale_max-scale_min) + scale_min;
}

inline void Mutable_Range::set_base(double x)
{
	base = gooey::math::fix_within_bounds(x, 0, 1);
	p_overflow = gooey::math::fix_within_bounds(x,
		std::numeric_limits<double>::lowest(),
		std::numeric_limits<double>::max());
}

inline void Mutable_Range::set(double x)
{
	x = gooey::math::fix_within_bounds(x, scale_min, scale_max);
	if (scale_max == scale_min) return;
	set_base ( (x - scale_min) / (scale_max-scale_min) );
}

inline void Mutable_Range::set_min(double x)
{
	scale_min = gooey::math::fix_within_bounds(x, 0, std::numeric_limits<double>::max());
	if (scale_min > get()) set(scale_min);
	if (scale_min > scale_max) scale_max = scale_min;
}

inline void Mutable_Range::set_max(double x)
{
	scale_max = gooey::math::fix_within_bounds(x, 0, std::numeric_limits<double>::max());
	if (scale_max < get()) set(scale_max);
	if (scale_min > scale_max) scale_min = scale_max;
}

inline double Mutable_Range::multiply(double multiplicand, bool overflow)
{
	if (base == 0) base = std::numeric_limits<double>::min();
	overflow ?	set_base (base * multiplicand)
			 :	set_base (p_overflow * multiplicand);
	return get();
}

inline double Mutable_Range::add(double addend, bool overflow)
{
	overflow ?	set_base (base + addend)
			 :	set_base (p_overflow + addend);
	return get();
}

inline double Mutable_Range::product_walk_probability(double p, double factor)
{
	if (factor == 0) return 0;
	if (factor < 0) factor = -1/factor;
	p = gooey::math::fix_within_bounds(p, 0, 1);
	return p*factor / ( p*factor + 1 - p);
}

inline double Mutable_Range::sum_walk_probability(double p, double addend_factor)
{
	p = gooey::math::fix_within_bounds(p, 0, 1);
	double out = ( addend_factor * (1-p) + p ) / ( addend_factor * (1-p) + 1 );
	return out < 0 ? 0 : out;
}

inline double Mutable_Range::product_walk(double factor)
{
	//set_base_probability
	if (base == 0) base = std::numeric_limits<double>::min();
	set_base ( product_walk_probability(base, factor) );
	return get();
}

inline double Mutable_Range::sum_walk(double addend_factor)
{
	//set_base_probability
	set_base ( sum_walk_probability(base, addend_factor) );
	return get();
}


inline Mutable_Range::operator double() const
{
 return get();
}

inline Mutable_Range Mutable_Range::from_json(nlohmann::json const & json)
{
	Mutable_Range x;
	x.base = std::stod((std::string)json["base"]);
	x.p_overflow = std::stod((std::string)json["p_overflow"]);
	x.scale_min = std::stod((std::string)json["scale_min"]);
	x.scale_max = std::stod((std::string)json["scale_max"]);
	return x;
}

inline Mutable_Range Mutable_Range::from_json(std::string const & json)
{
	auto j = nlohmann::json::parse(json);
	return from_json(j);
}
	
inline std::string Mutable_Range::to_json()
{
	std::string json = "";
	return json + "{"
		+ "\"base\":\"" + gooey::io::serialize(base) + "\","
		+ "\"p_overflow\":\"" + gooey::io::serialize(p_overflow) + "\","
		+ "\"scale_min\":\"" + gooey::io::serialize(scale_min) + "\","
		+ "\"scale_max\":\"" + gooey::io::serialize(scale_max) + "\""
		+ "}";
}

} /* end namespace gooey::lineage ****************************************** */

#endif
