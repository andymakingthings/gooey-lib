//
//  selection.hpp
//  gooey-lib
//

#ifndef gooey_lib_selection_hpp
#define gooey_lib_selection_hpp

#include <list>
#include <random>

/* let's not pollute the namespace! ***************************************** */
namespace gooey::lineage {

// takes a list of individuals, a fitness function, and the number of
// individuals to select; returns a list of iterators to the winners.
// this function TAKES OWNERSHIP and RETURNS OWNERSHIP.
template <typename T, typename Fn>
std::list<typename T::iterator> fitness_proportional_selection(T & list_individuals, Fn fitness_function, unsigned long long N_choose)
{
	// get the sum of all individuals' fitness
	decltype(fitness_function(*list_individuals.begin())) sum_fitness{0};
	for (auto &i : list_individuals) sum_fitness += fitness_function(i);
	
	std::random_device rd;
	std::mt19937 toss(rd());
	std::uniform_real_distribution<> dart_board(0, sum_fitness);
	
	std::list<typename T::iterator> winners_list;
	
	for (int i = 0; i < N_choose; ++i)
	{
		// choose an individual:
		// throw a dart—
		double dart = dart_board(toss);
		// find the individual at that location:
		decltype(fitness_function(*list_individuals.begin())) sum_search{0};
		for (typename T::iterator it = list_individuals.begin(); it != list_individuals.end(); ++it)
		{
			sum_search += fitness_function(*it);
			if (sum_search >= dart)
			{
				// dart found, add to the list
				winners_list.emplace_back(it);
				break;
			}
		}
	}
	
	return winners_list;
}


// takes a list of individuals, a fitness function, and the number of
// individuals to select; returns a list of iterators to the winners.
// this function TAKES OWNERSHIP and RETURNS OWNERSHIP.
template <typename T, typename Fn>
std::list<typename T::iterator> stochastic_universal_sampling(T & list_individuals, Fn fitness_function, unsigned long long N_choose)
{
	// get the sum of all individuals' fitness
	decltype(fitness_function(*list_individuals.begin())) sum_fitness{0};
	for (auto &i : list_individuals) sum_fitness += fitness_function(i);
	auto interval = sum_fitness/N_choose;
	
	std::random_device rd;
	std::mt19937 toss(rd());
	std::uniform_real_distribution<> dart_board(0, interval);
	double winner_location = dart_board(toss); // starting point
	double step_location = fitness_function(*list_individuals.begin());
	
	std::list<typename T::iterator> winners_list;

	typename T::iterator it = list_individuals.begin();
	for (int i{0}; i < N_choose; ++i)
	{
		// find the next winner:
		while (step_location < winner_location)
		{
			// step through the list of individuals:
			++it;
			step_location += fitness_function(*it);
		}
		
		// add the winner
		winners_list.emplace_back(it);
		
		// set location of the next winner
		winner_location += interval;
	}

	return winners_list;
}

// takes a list of individuals, and returns an iterator to the winner.
// this function TAKES OWNERSHIP and RETURNS OWNERSHIP.
template <typename T>
typename T::iterator random_selection(T & list_individuals)
{
	std::random_device rd;
	std::mt19937 choose(rd());
	std::uniform_int_distribution<unsigned long> random_individual(0, list_individuals.size()-1);
	double winner_location = random_individual(choose);
	
	// find our winner
	typename T::iterator winner = list_individuals.begin();
	for (int i = 0; i < winner_location; ++i) ++winner;
	
	return winner;
}


} /* end namespace gooey::lineage ****************************************** */

#endif
